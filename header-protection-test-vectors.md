---
title: Test Vectors for E-mail Header Protection
---

Here you can find test vectors for end-to-end cryptographic protections for e-mail.

They refer to the formats described in [`draft-ietf-lamps-header-protection`](https://datatracker.ietf.org/doc/draft-ietf-lamps-header-protection/).

# Screenshots

If you can take screenshots of any of these messages with your Mail User Agent (MUA) of choice, that would be great!
Don't worry if you can't get to every message -- any screenshots at all are better than none.

Even MUAs that offer no cryptographic capabilities would be good to see (at least for the `no-crypto*` messages and the `smime-multipart*` messages).

Here's how:

 * Start by preparing your MUA following the Preparation step below.
 
 * `git clone https://gitlab.com/dkg/lamps-header-protection.git`

 * In that checked-out git repository, make a directory for your MUA, and a subdirectory inside that with the version number.
   Inside the versioned folder, make a directory for screenshots of renderings and replies.
   For example: `mkdir -p screenshots/MailTool/3.14/{render,reply}`
   
 * Make a note or screenshot in your MUA's screenshot directory of the settings you used to configure it.
   Typically, this is either `config.png` or `README.md` (or both!).

 * Capture two screenshots per message, with each screenshot named after the message label.

   * One showing the message being rendered to the user (e.g., `screenshots/MailTool/3.14/render/smime-multipart-wrapped.png`)

   * The other shows the message composition window after the user tries to reply to the essage (e.g., `screenshots/MailTool/3.14/reply/smime-multipart-wrapped.png`)
   
 * Make a git commit and submit it [as a merge request](https://gitlab.com/dkg/lamps-header-protection/-/merge_requests) or send the screenshots and your notes in an [e-mail to the lead author](mailto:dkg@fifthhorseman.net?subject=draft-ietf-lamps-header-protection+screenshots) of the draft with `draft-ietf-lamps-header-protection screenshots` in the Subject line.

# Preparation

These messages use example LAMPS cryptographic objects found in [`draft-ietf-lamps-samples`](https://datatracker.ietf.org/doc/draft-ietf-lamps-samples/).

To be able to verify the signatures in these messages, you'll need to import and be willing to rely on the [LAMPS RSA Sample Certificate Authority](ca.rsa.crt) to identify the author of an e-mail message.

To be able to decrypt the encrypted messages, you'll need to import [Bob's PKCS12 object](bob.pfx) into your mail user agent.
This PKCS12 object is encrypted with the simple ASCII string `bob`.

# Online Access

The test vector messages can be accessed in a read-only IMAP mailbox at:

[`imap://bob@header-protection.cmrg.net/`](imap://bob@header-protection.cmrg.net/)

Any password should successfully authenticate to this account.

# Download Formats

The sample messages are also available for download in several different formats:

- [mbox](header-protection.mbox)
- [maildir (zipped)](header-protection.maildir.zip)
- Individual messages:

