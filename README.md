# Header Protection for End-to-End Encrypted E-mail

This repository contains the markdown source for the
[draft-ietf-lamps-header-protection](https://datatracker.ietf.org/doc/draft-ietf-lamps-header-protection/)
Internet Draft.

This draft should be discussed on [the LAMPS mailing
list](https://www.ietf.org/mailman/listinfo/spasm).
